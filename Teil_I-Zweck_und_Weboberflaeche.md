# Teil I: Einführung und GitLab-Weboberfläche

## Start

### Wer?
- Vorstellung Referentin (Inkscape, Inkscape-Erweiterungen, Beteiligung an vielen anderen Open-Source-Projekten)
- Hintergrund der Teilnehmer?

### Ziele
- Zweck: soll TN in die Lage versetzen, beim Hackathon mitzumachen
- nicht: detaillierte Erklärungen / Hintergründe!
- praktisch orientiert
- Jederzeit Fragen stellen!

## Überblick

- Teil I: 
   - Was ist, wozu braucht
   - allereinfachste Möglichkeit der Nutzung über Weboberfläche
   - erstes eigenes Projekt erstellen
   - Hausaufgabe!
- Teil II: 
   - Terminologie
   - Nutzung vom eigenen Computer aus
   - parallel grafisch mit VSCodium und über die Kommandozeile
- Teil III: 
   - Workflow
   - best practices / Dos + Don'ts
   - Probleme lösen
   - Cheatsheets

## Wozu braucht man das überhaupt?

Steine in den Weg legen? Nein! Denn: 

- Versionsverwaltung für Softwarequellcode 
- gemeinsamer, geteilter Ablageort, auf den mehrere Personen Zugriff haben
- Vereinfacht asynchrones Arbeiten + Remote-Arbeiten
- Hilft, bei der gemeinsamen Arbeit am selben Quellcode den Überblick zu behalten. Ermöglicht es, nebeneinander an verschiedenen neuen Funktionen zu arbeiten, und diese sauber voneinander zu trennen
- Ermöglicht es, zu sehen, wer wann was geändert hat
- Ermöglicht es, einzugrenzen, durch welche Codeänderung ein Fehler in die Software gekommen ist
- Ermöglicht es, Änderungen rückgängig zu machen (alle!)
- Wird in diesem Hackathon verwendet und erwartet :) 

## Was ist git?

Git ist eine Versionsverwaltung:

- freie Software, ursprünglich entwickelt von Linus Torvalds für die Entwicklung am Linuxkernel
- hohe Bekanntheit durch github, große Verbreitung (ca. 70% im OpenSource-Bereich - nach Zahlen von OpenHub.net), daher auch viele Tutorials etc.
- sehr effizient in der Art, wie Codeänderungen / Zweige gespeichert werden
- Nutzung normalerweise für Softwarequelltexte, geht aber auch für andere Textdateien (z.B. Wikis, als Backup-Lösung, CMS, Notizsammlung, etc.). Nicht zu große) Binärdateien (z.B. Bilder) können auch verwaltet werden
- feature-reich (quasi endlos, die meisten Funktionen wird man nicht oder nur in Ausnahmefällen benötigen). Üblicherweise Bedienung über Kommandozeile, grafische Werkzeuge verfügbar (git-gui, gitg, git-cola, Plugins für verschiedene Editoren etc. 
- verteilt, jeder Entwickler hat seine eigene, vollwertige Kopie, mit der er/sie arbeitet. Üblicherweise wird jedoch ein Ablageort zum offiziellen Ort bestimmt.

## Nahtlose Überleitung zu: GitLab!

GitLab ist eine Webserver-Software, die das gemeinsame Arbeiten an git-Projekten einfacher macht, indem sie:
- einen gemeinsamen Ablageort für die Dateien / Versionshistorie bietet
-  Git-Prozesse grafisch aufbereitet
- Code-Reviews mit Diskussionen ermöglicht
- die Bearbeitung von Dateien ermöglicht
- eine Außendarstellung des Projektes ermöglicht (hier: für die Jury, im echten Leben: für die Nutzer und eventuelle Beitragende)
- viele Funktionen zum Projektmanagement zur Verfügung stellt, z.B.:
  - Fehlertracker
  - Rechtemanagement
  - Wiki
  - automatische Builds und Tests

## Dein erstes Projekt auf GitLab

Ziel: Grobes Zurechtfinden in der Projektverwaltung, dafür: eigenes Projekt anlegen, erste Dateien anlegen

- Einloggen :) 
- Gemeinsam ein erstes eigenes Projekt anlegen: Name, Beschreibung
- Terminologie: Repo, repository, ‘Upstream’
- Evtl. zu mehreren eines? Rechte vergeben, Nutzer zulassen.
- Einigung: Sprache? Deutsch oder Englisch? Im OpenSource-Bereich ist Englisch üblich, steigert aber ggf. die Schwierigkeitsstufe. Hier wahrscheinlich Deutsch.
- Best practices: Dateien LICENSE und README.md als absolutes Minimum, bei Fremdkomponenten IMMER auf kompatible Lizenz achten
- Erste Dateien anlegen
- Terminologie: 
  - Commit - eine Sammlung von Codeänderungen, ein Schritt auf dem Weg zur neuen Funktion, ein Eintrag in der Versionshistorie
    - Wann? Immer, wenn ein Schritt abgeschlossen ist, oder ggf. auch, um den aktuellen Stand zu sichern, weil man was anderes vorhat
  - Zweig - eine ‘Code-Linie’. Ein Git-Repo kann eine große Anzahl an Zweigen haben, z.B. für verschiedene Versionen einer Software (z.B. falls diese weiter unabhängig voneinander unterstützt werden), oder auch für neue Funktionen und Fehlerkorrekturen. Diese Zweige kann man benennen, wie man möchte. Üblicherweise (und standardmäßig) heißt der Hauptzweig ‘master’ (auf GitHub: ‘main’), man könnte ihn aber auch ‘toastbrot’ nennen. Wenn man an etwas arbeitet, erstellt man dafür üblichweise einen neuen Zweig.
- Erste Dateien bearbeiten: Edit, IDE
  - Erneuter Commit
- Ordner anlegen: .gitkeep-Datei wird automatisch von GitLab angelegt, da Git nur Dateien (inkl. Pfad), nicht aber Ordner trackt. Datei könnte auch ‘Einhorn’ heißen. Gerne dran denken, diese Datei hinterher wieder zu entfernen!