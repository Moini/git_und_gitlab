# Git-Cheatsheet

## Git einrichten (Name, Emailadresse)
 
`$ git config --global user.name "Mein Name"`  
`$ git config --global user.email meine_email_adresse@provider.com`

## Neues Repository anlegen

1. `$ git init <Verzeichnisname>`
2. Dateien hinzufügen (`programm.py`, `README.md`, `LICENSE`)
3. `.gitignore` für Dateien und Ordner, die niemals in der Versionsgeschichte auftauchen sollen, z.B. Editor-Backup-Dateien (`~datei.name`), kompilierte Python-Dateien (`datei.pyc`), komplette build-Verzeichnisse, oder auch lokale Konfigurationsdateien (`localsettings.config`)
4. `.gitkeep` für leere Verzeichnisse

## Standardbefehle

`git clone <URL> [<Ordnername>]`:  
holt den Inhalt eines entfernten Repositoriums auf den eigenen Computer, legt eine unabhängige Kopie des Repos an

`git status`:  
zum Anzeigen des aktuellen Status (Zweig, geänderte Dateien, Dateien in der staging area), nützliche Hilfe! Oft anwenden!
  
`git <Kommando> --help`  
hilft immer!
   
`git diff`  
zeigt nicht committete Änderungen im aktuellen Arbeitszweig an
    
`git diff --staged`  
zeigt alle Änderungen in Dateien an, die demnächst 'committed' werden sollen (als nächste Revision gespeichert werden soll)
   
`git diff <neuer Hash> ... <alter Hash>`  
vergleicht zw. bestimmten Versionen
  
`git add <Dateiname> [<Dateiname>]`  
fügt geänderte + neue Dateien der staging area hinzu (zum Commit vorgemerkt)
  
`git commit (-a) (-S) -m "Eine Beschreibung der Änderung, ggf. mit fixes #<Bugnummer>."`  
macht eine neue Revision.  
Mit `-a`: alle geänderten Dateien werden automatisch hinzugefügt (aber keine neu angelegten!). Diese Option hilft auch, wenn man Dateien gelöscht hat, ohne Git 'Bescheid zu geben'.
Mit `-S`: signieren mit GPG  
Mit `--author "Name <name@email.com>"`: anderen Autor des Commits angeben, wird getrennt vom Committer gespeichert
    
`git log`  
Revisionshistorie, zur Anzeige der Commits, Datum und Hashwert (dient zur Identifikation)

`git log | head` (Linux)
zeigt (so ungefähr) den letzten Commit an

`git remove <Dateiname>`  
für das Löschen von Dateien (echtes Löschen von der Platte und für den nächsten Commit). Geht nur, wenn die Datei nicht bereits gelöscht ist!

`git mv <alter.name> <neuer.name>`  
für das Umbenennen von Dateien, damit Git das sofort kapiert (auch bei auf andere Art umbenannten Dateien merkt es das oft, aber nicht immer).  
Funktioniert nur, wenn die Datei noch den Originalnamen trägt!

`git pull`  
Informationen über Änderungen an allen entfernten Zweigen holen. Änderungen am aktuellen Zweig auch anwenden.   
Im Ergebnis wird der aktuelle Zweig aus dem entfernten Repo aktualisiert.  
Dabei wird, falls beide Zweige sich geändert haben, ein 'merge commit' erstellt.  
Mit `git pull --rebase` wird versucht, auf einen solchen zu verzichten, und die eigenen Commits linear an die aus dem Hauptzweig anzuhängen.   
`git pull` am besten nur machen, wenn alles schön committed ist (sonst weigert sich git ggf. auch einfach).

`git fetch (-p)`  
Informationen über Änderungen im Repo mit dem Namen 'origin' holen, aber nicht anwenden 
`-p`: löscht lokale Kopien von Zweigen, die remote nicht mehr existieren
   
`git push`  
die neuen Commits in das entfernte Repository übertragen
    
`git push -f`  
neue Commits in das entfernte Repo übertragen und dabei keine Rücksicht nehmen, auf das, was dort schon ist.  
Überschreibt dort den Zweig. Nie anwenden!      
Außer: es hat niemand anderes Zugriff auf den Zweig als Du selbst.

`git branch <neuer_Zweigname>`  
erstellt einen neuen Zweig im eigenen lokalen Repo

`git checkout <Zweigname>`  
wechselt zu entsprechendem Zweig. Dabei ändern sich die gespeicherten Dateien auf der Festplatte. 
  Git weigert sich, zu wechseln, wenn im aktuellen Zweig noch Änderungen sind, die nicht committed wurden.
(`git stash` kann in diesem Fall auch verwendet werden, hierzu bitte Handbuch lesen)

`git checkout -b <Zweigname>`  
legt einen neuen Zweig an und wechselt dort hin       

## Fehler korrigieren

`git checkout Dateiname`  
einzelne, noch nicht gestagete Datei auf Stand des letzten Commits zurücksetzen (Änderungen gehen verloren)

`git checkout -- .`  
alle noch nicht zum Commit vorgemerkten Dateien auf den Stand des letzten Commits zurücksetzen (Änderungen gehen verloren)

`git checkout -p`  
    interaktiv noch nicht zum Commit vorgemerkte Dateien auf den Stand des letzten Commits zurücksetzen (Änderungen gehen verloren)

`git reset HEAD <Dateiname>`  
entfernt eine Datei aus der Staging area (Änderungen bleiben erhalten)
    
`git reset HEAD -- .`  
entfernt alle Dateien aus der Staging area (Änderungen bleiben erhalten)

`git clean -i `
interaktiv ungetrackte Dateien entfernen  
erzwingen: statt `-i` mit `-f` für Mutige  
`-d`: leere Verzeichnisse auch löschen

`git reset HEAD~`  
letzten Commit rückgängig machen
  
## Umgang mit Remotes
  
`git remote -v`  
um sich alle aktuellen Remotes anzeigen zu lassen (bei lokal erzeugtem Repo zunächst leer)

`git remote add <Name> <URL>`  
git über ein Remote für das aktuelle Projekt informieren. Üblicher Name für eigenes Ursprungsrepo ist 'origin'
    
`git push --set-upstream <Repo-Name des entfernten Repos> <neuer Zweigname>`  
wenn der aktuelle lokale Zweig keine Entsprechung im entfernten Repo hat, wird er so dort angelegt (damit man danach einen Merge Request machen kann)

## Workflows

### Änderungen von einem Zweig in einen anderen übernehmen (Merge)

1. Falls notwendig, zum Zielzweig wechseln:  
`git checkout <Zweigname>`  
2. Die Änderungen des anderen Zweiges in den aktuellen Zweig übernehmen:  
`git merge (--ff-only) (--Xtheirs) <anderer Zweigname>`  
`--ff-only`: dabei keinen Merge-Commit machen, bricht ab, wenn dies nicht möglich ist  
`--Xtheirs`: im Zweifel immer die Änderung des anderen Zweiges übernehmen  
3. Ggf. Merge-Konflikte lösen:  
den Anweisungen von Git auf der Kommandozeile folgen und betroffene Dateien editieren;  
siehe z.B. https://docs.gitlab.com/ee/university/training/user_training.html

### Eigenen Zweig auf dem aktuellen Stand halten

1. lokal arbeiten, am eigenen Zweig
2. `git pull --rebase origin <branchname, meist ‘master’>`  
aktualisiert den lokalen Zweig auf den Stand des entfernten Repos. Ändert dabei die lokale History.  
Vor einem push in ein Repo oder einen Zweig, der mit anderen geteilt wird ausführen!
Auch gerne vor einem Merge Request ausführen.
    Ohne `--rebase`: erzeugt merge commits ('update to master'), nicht immer gern gesehen!
  

### Standardworkflow mit GitLab

1. Einen 'Issue' mit einer Beschreibung des zu lösenden Problems in der Oberfläche anlegen
2. Lokal einen Zweig anlegen, dessen Name sich auf den Bug bezieht:  
`git checkout -b branch_name`
3. Lokal arbeiten
4. Geänderte und neue Dateien zum Commit vormerken  
`git add <file> <file>`
5. Committen:   
`git commit -m "message"`    
(ggf. mit `-S` signieren, oder mit `-a` alle geänderten Dateien automatisch mit committen, ohne dass sie vorher in der Staging area waren - Achtung: NEUE Dateien müssen immer manuell mit git add hinzugefügt werden!)
6. eventuell mehrere Commits vor dem nächsten Schritt
7. eigenen Zweig zu GitLab verschicken:  
`git push --set-upstream origin <neuer Zweigname, am besten derselbe wie aktuell lokal>` 
9. Auf GitLab einen Merge Request machen:  
    Optionen beachten, z.B.:  
    - Zweig nach dem Merge löschen: hält das Repo sauber  
    - Commits zu einem einzigen zusammenfassen ('squash'): hält die History klein und sauber  
    - Bearbeitungen durch andere erlauben: nötig für Rebase in GitLab durch jemand anderen  
    - 'WIP:' im Titel: verhindert Merge, ermöglicht es aber anderen, die eigenen Änderungen anzusehen / sich diese zu holen  
10. Wenn der Zweig in GitLab noch nicht von anderen verwendet wird, kann man ihn auch noch lokal auf einen anderen Zweig aktualisieren (z.B. `git merge --rebase master`) und mit erzwungenem Push überschreiben (`git push -f`). So können andere die Änderungen verfolgen, während man seinen Zweig aktuell hält.

## Sondertricks

### diff mit Punkten als Indikator für Tabulatoreinrückungen
`git diff --color | sed 's/\t/.       /g' | less -R` (Linux)

### Einen Fork aktualisieren (wenn man keine Push-Erlaubnis für das Originalrepo hat)
`git remote --add <Name, z.B. upstream> git@website.com:gruppe/projekt.git`  
`git pull --rebase <Name, z.B. upstream> <zweigname>`

### Commits zusammenfassen
`git rebase --interactive 12345^`  
Dabei "12345" durch den hash des ersten Commits ersetzen den du neu schreiben möchtest.

### Submodul aktualisieren (auf eine aktuellere Version als den aktuellen Stand im Hauptrepo)
`git submodule update --remote`

### Liste aller Commits, die in ‘zweig’ sind, aber nicht in ‘ast’
`git log ast..zweig --oneline`

### dasselbe in hübsch
`git log ast..zweig --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%aD)%C(reset) %C(white)%s%C(reset) %C(dim white) - %an%C(reset)%C(bold yellow)%d%C(reset)'`

### Autoren aller Commits im Zweig 'zweig' auflisten und alphabetisch sortiert in eine Datei ausgeben
`git log zweig --pretty=format:"%an" | sort -u > /tmp/authors.txt` (Linux)

### Modifizierte Dateien eines Commits anzeigen
`git show --name-only 123456`

### Anzeigen, in welchen Zweigen ein bestimmter Commit enthalten ist
`git branch -a --contains 123456`

## Allgemeine Infos

- **4 Zustände/Ebenen**, in denen sich Änderungen befinden können: 
   - **Arbeitsbaum**: was man gerade bearbeitet
   - **staged**: zum Commit vorgemerkt
   - **committed**: in der Versionsgeschichte drin, mit Infotext
   - **stashed**: nicht committed, temporär aus dem Weg geräumt, aber nicht vergessen

## Glossar
   
**branch** (Zweig):  
eine ‘Code-Linie’. Ein Git-Repo kann eine große Anzahl an Zweigen haben, z.B. für verschiedene Versionen einer Software (z.B. falls diese weiter unabhängig voneinander unterstützt werden), oder auch für neue Funktionen und Fehlerkorrekturen. Diese Zweige kann man benennen, wie man möchte. Üblicherweise (und standardmäßig) heißt der Hauptzweig ‘master’, man könnte ihn aber auch ‘toastbrot’ nennen. Wenn man an etwas arbeitet, erstellt man dafür üblichweise einen neuen Zweig.  
Zweige können gewechselt werden. Wenn man dies tut, ändern sich die Dateien auf der Festplatte automatisch, um den Stand des aktuell aktiven Zweiges darzustellen. Es wird keine neue vollständige Kopie angelegt, wie in anderen Versionsverwaltungsprogrammen.

**commit** (ein einzelner Eintrag in der Versionshistorie):  
Wenn jemand eine neue Funktion oder eine andere Änderung abgeschlossen hat (bei größeren Änderungen natürlich auch nur Teilschritte), dann wird er diese speichern wollen. message/Beschreibung, Datum, Autor, Committer, Emailadresse, die Änderungen sowie ein Hashwert und der Hashwert des vorigen commits werden gespeichert. Jeder commit bekommt damit eine eindeutige ID und Reihenfolge in der Kette der commits. Jeder neue Hashwert basiert auf der kompletten Geschichte des Zweiges, so dass Fälschungen durch Einfügen eines zusätzlichen Commits oder ändern eines älteren Commits auffallen würden (wie bei der Blockchain/Bitcoin, ‘Hashbaum’). Zusätzlich können Commits von ihrem Autor per GPG signiert werden.

**diff**:  
Darstellung der Unterschiede zw. zwei verschiedenen Commits / Zweigen / dem aktuellen Arbeitsverzeichnis und dem vorigen Commit
    
**merge** (Zusammenführen von Zweigen):  
Übernahme der Änderungen in einem Zweig in einen anderen
    
**merge request**:  
Bitte um Aufnahme eigener Änderungen in einen anderen Zweig

**repository** (Repositorium):  
Speicherort für den Code. Ein Repositorium ist ein Verzeichnis mit Dateien, die unter Versionskontrolle sein sollen, und einem Unterverzeichnis namens .git darin. 

**remote**:  
entferntes Repositorium, z.B. im GitLab oder im Internet oder beim Kollegen im internen Netzwerk

**staging area** (vorgemerkt zum Commit):  
die ‘Vorhölle’ für geänderte Dateien. Alles, was hier vorgemerkt ist, wird auch beim commit mit gespeichert. Alle anderen Änderungen nicht. So kann man z.B. größere Änderungen in mehrere Commits aufteilen, oder andere Änderungen einfach nicht übernehmen.

**stash** (in den Vorratsschrank packen):  
Änderungen für spätere Verwendung in die Abstellkammer stellen