# Git- + GitLab-Workshop: Vorbereitungen für Teil 2

- [ ] Installation von **Git**:  
    a) Debian/Ubuntu: `apt install git-all`  
    b) macOS: `git` eintippen, oder [besser eine aktuelle Version installieren](https://www.atlassian.com/de/git/tutorials/install-git#mac-os-x)  
    c) Windows: [Git for Windows](https://git-for-windows.github.io/)
- [ ] [**VSCodium** installieren](https://vscodium.com/)
- [ ] VSCodium **konfigurieren**:
  - [ ] **Telemetrie und andere Kontaktaufnahmen zu Microsoft deaktivieren**: `File → Preferences : Online Services Settings` , dann alles deaktivieren außer `Extensions → Auto-check Updates` ([Mehr Info](https://github.com/VSCodium/vscodium/blob/master/DOCS.md#disable-telemetry))
  - [ ] **Crash reporting an Microsoft deaktivieren**: Strg + Umschalt + P : `Preferences : Configure Runtime Arguments`, in der Datei `"enable-crash-reporter": false` setzen
  - [ ] **Git aktiviert?** `File → Settings → Preferences : Git: Enabled` aktivieren (falls nicht aktiviert)
  - [ ] **'Git History'**-Plugin + **'GitLab Workflow'**-Plugin installieren
  - [ ] **Gitlab-Instanz mitteilen**: `File → Settings → Extensions → GitLab Workflow: Instance URL: Edit in settings.json`, dann in der Datei zw. den leeren Anführungszeichen `https://git.chaotikum.org` eintragen
  - [ ] **Token für 'GitLab Workflow'-Plugin** [generieren](https://git.chaotikum.org/-/profile/personal_access_tokens) mit den Rechten `api` + `read_user`. Dann in VSCodium eintragen: Strg + Umschalt + P: `Gitlab: Set GitLab personal access token`. Zuerst die URL `https://git.chaotikum.org`, mit Enter bestätigen, dann das Token eingeben.
- [ ] **GitLab-Account** anpassen:
  - [ ] E-Mail-Adresse verwenden, die **öffentlich sichtbar** sein darf! GitLab bietet sowas auch an [in der Oberfläche](https://git.chaotikum.org/-/profile) unter `Commit email`. Ggf. auch diese E-Mail-Adresse in Git (im Terminal) konfigurieren mit `git config --global user.email "<komplizierte_e-mailadresse>@git.chaotikum.org"`. Zeitzone und evtl. Profilbild einstellen.
  - [ ] optional: **SSH-Schlüssel** generieren und öffentlichen(!) Schlüssel [in GitLab hinterlegen](https://git.chaotikum.org/-/profile/keys) ([Deutsche Anleitung](https://www.heise.de/tipps-tricks/SSH-Key-erstellen-so-geht-s-4400280.html), [Englische Anleitung, passend für GitLab](https://git.chaotikum.org/help/ssh/README#generate-an-ssh-key-pair) (spart den Login mit Benutzername/Passwort bei der Verwendung von Git auf der Kommandozeile)
  - [ ] optional: **GPG-Schlüssel** generieren und öffentlichen(!) Schlüssel [in GitLab hinterlegen](https://git.chaotikum.org/-/profile/gpg_keys) (ermöglicht das Überprüfen von Signaturen neuer Codebeiträge) (Anleitungen (Deutsch): [Linux](https://wiki.ubuntuusers.de/GnuPG/), [Windows](https://gnupg.com/20200228-erste-schritte-gpg4win.html), [macOS-Download](https://gpgtools.org/), [macOS-Anleitung](https://www.datenschutz.rlp.de/fileadmin/lfdi/Dokumente/OH_GPGTools_fuer_Mac.pdf); [Englisch, passend für GitLab](https://git.chaotikum.org/help/user/project/repository/gpg_signed_commits/index.md))
- [ ] optional: anständigen **Dateimanager** installieren (am besten zweispaltig, z.B. krusader (Linux), TotalCommander (Windows), DoubleCommander, muCommander (Linux, Windows, macOS))
