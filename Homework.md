# Git + GitLab Workshop: Preparations for Part 2 


### Initial Installation:

- [ ] Installation of **Git**: 
    - Debian/Ubuntu: `apt install git-all` 
    - macOS: type `git` , or better [install a current version][install git on mac] 
    - Windows: [Git for Windows] 
- [ ] Install **[VSCodium]** 
- [ ] Optionally: install a decent **file manager** - one with two columns is optimal,  
e.g. krusader (Linux), TotalCommander (Windows), DoubleCommander, muCommander (Linux, Windows, macOS)


[install git on mac]: https://www.atlassian.com/git/tutorials/install-git#mac-os-x
[Git for Windows]: https://git-for-windows.github.io/
[VSCodium]: https://vscodium.com/
[codium-disable-telemetry]: https://github.com/VSCodium/vscodium/blob/master/DOCS.md#disable-telemetry
[code-disable-telemetry]: https://code.visualstudio.com/docs/supporting/faq#_how-to-disable-telemetry-reporting
[gitlab-vscode-extension-settings]: https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/README.md#step-2-add-token-to-gitlab-workflow-extension


### Configure VSCodium: 

- [ ] **Disable telemetry and other contacts to Microsoft**:  \
"File → Preferences > Online Services Settings", then disable everything except "Extensions: Auto Check Updates". More info: [codium-disable-telemetry]
- [ ] **Disable crash reporting to Microsoft**:  \
Ctrl + Shift + P : `Preferences: Configure Runtime Arguments` , set in the file `"enable-crash-reporter": false`
- [ ] **Enable Git**  \
"File → Preferences > Settings" (Ctrl + ,) : `Git: Enabled` , tick it to enable 
- [ ] Install **'Git History'** plugin + **'GitLab Workflow'** plugin 
- [ ] **Set GitLab instance**:  \
  "File → Preferences > Settings" → "Extensions > GitLab Workflow → Instance URL: Edit in settings.json", then enter in the file in a new line `"gitlab.instanceUrl": "https://gitlab.com"` . More info: [gitlab-vscode-extension-settings] 
- [ ] **Generate [access token] for 'GitLab Workflow' plugin** with the rights `api` + `read_user`.  \
    Then in VSCodium: Ctrl + Shift + P : `Gitlab: Set GitLab personal access token` → First confirm the URL `https://gitlab.com` with Enter, then enter the token. 

*Note:* In the gitlab links used here, put your gitlab instance's link in place of "gitlab.com"


### Customize **GitLab account**:

- [ ] Use an email address that can be **publicly visible**! 
  For GitLab, it's under "Edit [profile] : Commit email". Set time zone and possibly profile picture.  \
  If necessary, also configure this e-mail address in Git (via the terminal) with `git config --global user.email "your_email@example.com"` . More info: [configuring-git][git-config]
- [ ] optional: Generate **SSH key** and store public(!) Key in GitLab under "profile → [SSH keys]" (saves the login with username/password when using Git on the command line)  \
Tutorials: [Gitlab docs][SSH gl] ; [Windows-10][SSH win] ; [older windows][SSH win-old] ; Software for: [Windows][SSH win dnload]
- [ ] optional: Generate **GPG key** and store public(!) Key in GitLab under "profile → [GPG keys]" (allows you to verify signatures of new code posts)  \
Tutorials: [GitLab docs][GPG gl] ; Software for: [Windows][GPG win dnload] ; [macOS][GPG mac dnload]


[access token]: https://gitlab.com/-/profile/personal_access_tokens
[profile]: https://gitlab.com/-/profile
[SSH keys]: https://gitlab.com/-/profile/keys
[GPG keys]: https://gitlab.com/-/profile/gpg_keys

[git-config]: https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup#_your_identity
[SSH gl]: https://gitlab.com/help/ssh/README#generate-an-ssh-key-pair
[SSH win]: https://phoenixnap.com/kb/generate-ssh-key-windows-10
[SSH win-old]: https://phoenixnap.com/kb/generate-ssh-key-windows-10#ftoc-heading-5

[SSH win dnload]: https://www.chiark.greenend.org.uk/~sgtatham/putty/
[GPG win dnload]: https://gpg4win.org/
[GPG mac dnload]: https://gpgtools.org/

[GPG gl]: https://gitlab.com/help/user/project/repository/gpg_signed_commits/index.md

<!-- though following links are orphan, they contain useful information. so, avoid removing them without replacement with equivalent article in english -->
[GPG linux german]: https://www.translatetheweb.com/?from=de&to=en&a=https://wiki.ubuntuusers.de/GnuPG/
[GPG windows german]: https://www.translatetheweb.com/?from=de&to=en&a=https://gnupg.com/20200228-erste-schritte-gpg4win.html
