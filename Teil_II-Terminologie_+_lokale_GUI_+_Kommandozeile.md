# Teil II: Terminologie, lokale Repos managen mit grafischer Oberfläche und der Kommandozeile

## Git-Terminologie

**repository, Repo**:  
*Ein Repositorium bzw. Speicherort für den Code.*  
Ein Repositorium ist ein Verzeichnis mit Dateien, die unter Versionskontrolle sein sollen, und einem Unterverzeichnis namens `.git` darin. 

**Commit**:  
*Ein einzelner Eintrag in der Versionshistorie.*  
Wenn jemand eine neue Funktion oder eine andere Änderung abgeschlossen hat (bei größeren Änderungen natürlich auch nur Teilschritte), dann wird er diese speichern wollen. message/Beschreibung, Datum, Autor, Committer, Emailadresse, die Änderungen sowie ein Hashwert und der Hashwert des vorigen commits werden gespeichert.  
Jeder Commit bekommt damit eine eindeutige ID und Reihenfolge in der Kette der Commits. Jeder neue Hashwert basiert auf der kompletten Geschichte des Zweiges, so dass Fälschungen durch Einfügen eines zusätzlichen Commits oder Ändern eines älteren Commits auffallen würden (wie bei der Blockchain/Bitcoin, ‘Hashbaum’). Zusätzlich können Commits von ihrem Autor per GPG signiert werden.  
Befehl: `git commit -m “Commitnachricht”`

**branch**:  
*Ein Zweig bzw. eine ‘Code-Linie’.*  
Ein Git-Repo kann eine große Anzahl an Zweigen haben, z.B. für verschiedene Versionen einer Software (z.B. falls diese weiter unabhängig voneinander unterstützt werden), oder auch für neue Funktionen und Fehlerkorrekturen.  
Diese Zweige kann man benennen, wie man möchte. Üblicherweise (und standardmäßig) heißt der Hauptzweig ‘master’, man könnte ihn aber auch ‘toastbrot’ nennen. Wenn man an etwas arbeitet, erstellt man dafür üblichweise einen neuen Zweig.  
Zweige können gewechselt werden. Wenn man dies tut, ändern sich die Dateien auf der Festplatte automatisch, um den Stand des aktuell aktiven Zweiges darzustellen. Es wird keine neue vollständige Kopie der Dateien angelegt, wie in anderen Versionsverwaltungsprogrammen.
Zweignamen dürfen keine Leerzeichen oder Schrägstriche, Fragezeichen, Doppelpunkte u. einiges mehr enthalten. Auf der sicheren Seite ist man durch Beschränkung auf A-Z, a-z, Bindestrich und Unterstrich.

**Klonen**:  
*Eine Kopie eines anderen Repos erstellen.*   
Befehl: `git clone https://...` oder `git clone git@server.com:reponame.git` (https bzw. SSH)

**Auschecken / Checkout**:  
*Einen Zweig auswählen, an dem man arbeiten möchte.*  
Magie! Hierdurch ändern sich die Dateien auf der Festplatte!  
Es wird also nicht eine Kopie in einem anderen Ordner angelegt, sondern alles bleibt an Ort und Stelle.  
Achtung bei Anwendungen, die eine Datenbank verwenden! (bei inkompatiblen Änderungen des Datenbankschemas)  
Befehl: `git checkout zweigname`, oder neuer Zweig mit `git checkout -b neuer_zweigname`

**Pull**:  
*Änderungen aus dem entsprechenden Zweig des entfernten Repos holen und auf den aktuellen Zweig anwenden.*  
Entspricht standardmäßig `git fetch` + `git merge --rebase`.  
Befehl: `git pull`

**Fetch**:  
Informationen über die Änderungen aus dem entfernten Repo holen, ohne sie anzuwenden.  
Befehl: `git fetch`

**Diff**:  
*Darstellung der Unterschiede zw. zwei verschiedenen Commits / Zweigen / dem aktuellen Arbeitsverzeichnis und dem vorigen Commit.*  
Befehl: `git diff` bzw. `git diff --staged`

**Statusabfrage**:  
*Auf welchem Zweig bin ich grade? Welche Dateien sind neu / gelöscht / geändert? Sind diese schon zum Commit vorgemerkt?*  
Befehl: `git status`

**Stage**:  
*Vormerken zum Commit.*  
Die ‘Vorhölle’ für geänderte Dateien. Alles, was hier vorgemerkt ist, wird auch beim Commit mit gespeichert. Alle anderen Änderungen nicht. So kann man z.B. größere Änderungen in mehrere Commits aufteilen, oder andere Änderungen einfach nicht übernehmen.  
“Diese Datei ist so okay. Weitere Änderungen kann ich trotzdem machen, dann entweder wieder vormerken oder rückgängig machen.”  
Befehl: `git add dateiname dateiname ...`

**Push**:  
*Eigene Änderungen an das entfernte Repo schicken.*    
Befehl: `git push`
   
**Merge**:  
*Zusammenführen von Zweigen:*  
Übernahme der Änderungen eines anderen Zweiges in den aktuellen.  
Befehl: `git merge anderer_Zweig`

**Merge-Konflikt**: 
*Wenn Änderungen in beiden Zweigen dieselbe Stelle betreffen.*  
Welche soll nun ins Ergebnis übernommen werden?  
Kommandozeile: Git zeigt einem an, welche Dateien betroffen sind. Diese müssen dann so bearbeitet werden, dass sie inhaltlich / funktional sinnvoll sind. Problemstellen sind folgendermaßen markiert:  
```
1 <<<<<<< HEAD  
2 Lokaler Inhalt.  
3 =======  
4 Inhalt des anderen Zweigs.  
5 >>>>>>> anderer_zweig  
```

**Merge Request (MR)**:  
*Bitte um Aufnahme eigener Änderungen in einen anderen Zweig.*  
Auf GitHub auch: Pull request (PR)

**Stash**:  
*In den Vorratsschrank packen, unfertige Änderungen für spätere Verwendung in die Abstellkammer stellen.*  
So kann man auch ohne Commit schnell an etwas anderem arbeiten.

## Ausprobieren!

Einmal alles durchspielen, jeweils in VSCodium + Kommandozeile:

1. Repo holen oder anlegen (mit https-Adresse)
2. Nutzername und E-Mail konfigurieren:
   - einfach + lokal: Datei .git/config in verstecktem Ordner bearbeiten, folgendes anfügen:
    ```
    [user]
     name = Vorname Nachname
     email = email@domain.com
    ```
    Achtung: **Dateien sind versteckt**.  
    Unter **Windows**: unten in der Taskleiste den Dateibrowser öffnen. Dort den Ordner wählen, in dem das Git-Repo liegt. Dann auf `Ansicht > Optionen > Ordner und Suchoptionen ändern > Reiter Ansicht : Option 'ausgeblendete Dateien, Ordner oder Laufwerke anzeigen' aktivieren`.  
    Unter **Linux**: `Rechtsklick > Versteckte Dateien anzeigen`.  
    Unter **macOS**: Tasten `Command + Umschalt + .` im Finder drücken.  
    Falls Windows oder der Git-Credential-Manager nach den Login-Daten für git.chaotikum.org fragt, bitte auf 'Abbrechen' klicken. Es ist **nicht nötig**, diese anzugeben. Das Token reicht aus, und die Eingabe über ein weiteres System kann evtl. Probleme verursachen.
   - Kommandozeile (bzw. unter Windows mit 'Git CMD' aus dem Menü), global für das ganze System:
   ```
   git config --global user.name "Vorname Nachname"
   git config --global user.email email@domain.com
   ```
   (lokal geht auch, dazu erst in den richtigen Ordner navigieren und `--global` weglassen)
3. Status anzeigen - `git status` / VSCodium: Auf Git-Logo links klicken, dann auf das Aktualisieren-Icon klicken
4. History anzeigen - `git log` / VSCodium: Auf Uhr-Icon im Git-Bereich klicken
5. Zweig wählen - `git checkout <Zweig>` / VSCodium: unten links (Statusleiste) auf Zweignamen klicken und auswählen
6. Status anzeigen - `git status`/ VSCodium: Auf Git-Logo links klicken, dann auf das Aktualisieren-Icon klicken
7. Neuen Zweig machen und auswählen - `git checkout -b <Neuer_Zweigname>` / VSCodium: *...> Branch > Create Branch*
8. Status anzeigen - `git status` / VSCodium: Auf Git-Logo links klicken, dann auf das Aktualisieren-Icon klicken
9. An Dateien arbeiten
10. Diff anschauen - `git diff` / VSCodium: Auf einzelne Dateien im Git-Bereich klicken
11. Zur Übernahme vormerken (stagen) - `git add <Datei>`, `git rm <Datei>` / VSCodium: Auf + (add) hinter Dateiname im Git-Bereich klicken / Datei löschen (rm)
12. Doch nicht zur Übernahme vormerken - `git reset HEAD <Datei>` / VSCodium: Auf - hinter Dateiname im Git-Bereich klicken
13. Alle Änderungen an einer Datei rückgängig machen - `git checkout -- <Datei>` / VSCodium: hinter dem Dateinamen im Git-Bereich auf den gebogenen Pfeil klicken (discard changes), Warnung bestätigen
14. Diff der vorgemerkten Dateien anschauen - `git diff --staged` / VSCodium: Dateinamen im Git-Bereich einzeln anklicken
15. Restliche Dateien committen, gute Commit-Nachricht - `git commit -m "Commitnachricht"` / VSCodium: Im Feld 'Message' im Git-Bereich eine Nachricht eingeben. Mit Strg + Enter bestätigen.
16. Push - `git push --set-upstream <Repo-Name des entfernten Repos> <neuer Zweigname>`  
    oder in VSCodium im Git-Bereich: *... > Branch > Publish Branch*

Mögliche Probleme beim Push:

- Änderungen im Upstream-Zweig - Lösung: auf anderen Zweig schicken ODER lokal rebase (ODER lokal merge)
- Keine Berechtigung - Lösung: Berechtigungen erbitten, SSH-Schlüssel hinterlegen
- SSH-Probleme - Lösung: Googlen... 
