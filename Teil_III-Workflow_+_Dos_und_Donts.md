# Teil III: GitLab-Workflow und nützliche Tipps

## Möglicher Arbeitsablauf:

Darauf ausgelegt, Zusammenarbeit und Code-Qualität zu fördern, Merge-Konflikte zu vermeiden und anderen Beitragenden keine Probleme zu bereiten.

1. Einen **Issue** anlegen für das aktuelle Problem / den Programmfehler. Ggf. hier besprechen, was man vorhat.
2. Einen **Zweig** für aktuelle Arbeit erstellen:  
  `git checkout -b “improve_icons”`  
  (oder `git branch improve_icons; git checkout improve_icons`)
3. Am Zweig **arbeiten**
4. Lokal **testen**, ob das Ergebnis auch funktioniert (Software bauen, benutzen, ggf. Tests schreiben)
5. Wenn man zufrieden ist, Codeänderungen **nochmal durchgucken**:  
   `git diff`
6. Neue Dateien dabei? Dann:  
   `git add Dateiname Dateiname ...`
7. Alles klar? Alles soll mit rein? Dann:   
   `git commit -a -m “Improved icons for the app with better contrast”`
8. Nun neuen Zweig ins Repo **hochladen** (legt den Zweig dort auch an):  
   `git push --set-upstream origin improve_icons`
9. Prüfen: Ist schon alles fertig? Wenn nicht, dann:
   wiederhole 2., 3., 4., 5., 6., dann:  
   `git push`
10. Wenn ja, dann sollen die Änderungen wahrscheinlich **in den Hauptzweig**, daher:  
   Repo im GitLab aufrufen, Zweig wählen, einen **Merge Request machen**. Kurz aufschreiben, worum es geht.
11. Jetzt muss noch jemand **drüberschauen** und mergen. Kann man im Zweifel auch selbst.
12. Ggf. gibt es **Merge-Konflikte**, die jemand lokal auflösen muss.
13. Ggf. kann man den Zweig auf GitLab auch automatisch **auf den aktuellen Stand bringen** lassen (rebase). GitLab sagt einem das.

## Dos:

- immer einen Zweig pro Feature
- Hauptzweig sollte immer funktionsfähig sein
- Vor einem Commit oder Merge immer nochmal die Änderungen anschauen
- Debug-Code vor dem Commit entfernen
- Häufig den aktuellen Status abrufen
- Vor einem Push prüfen, ob es in der Zwischenzeit Änderungen im Zielzweig gab. Probleme lösen.

## Don'ts:

- Nichtssagende Commitnachrichten ('Updated file xyz', stattdessen: “Add Save button to details view”)
- Direkt in den gemeinsamen Hauptzweig pushen
- Force push gemeinsam genutzte Zweige (außer, alle wissen Bescheid und keiner weiß eine andere Lösung, ggf. fragen!)
- Fehlende Lizenz
- Dateien, die andere nicht brauchen, ins öffentliche Repo laden (stattdessen: `.gitignore`!)
- Passwörter, API-Keys etc. ins öffentliche Repo laden (stattdessen: Konfigurationsdatei)

## Cheatsheets:

* [Cheatsheet für Git-Kommandozeile](https://git.chaotikum.org/moini/git_und_gitlab/-/blob/master/Git-Cheatsheet.md)
* [Cheatsheet für Emojis](https://www.webfx.com/tools/emoji-cheat-sheet/)
* [Cheatsheet für Nachrichtenformatierung in GitLab](https://docs.gitlab.com/ee/user/markdown.html)